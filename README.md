# Reversi IA
Nom : Bescop
Prénom : Justin

# ETAT :
Joueur Random OPERATIONNEL
Evaluation du score : différence de score entre noir et blanc
Joueur Minmax OPERATIONNEL
Joueur AlphaBeta OPERATIONNEL
Tentative de nouvelle fonction d'evaluation mais pas assez de temps pour tester l'efficacité

# Problèmes rencontrés
Minmax : il faut pouvoir explorer toutes les possibilités avec une vision de 5 coups en avance
par exemple. 

Pb REGLE :~~J'ai un soucis à un moment donné l'IA Minmax passe son tour alors qu'elle doit jouer~~

Pb REGLE :~~Mon alphabeta semble bizarre. Il s'éxecute très rapidement en opposition au minmax et ne gagne pas souvent contre random.
Sur 100 parties il gagne tout juste un peu plus de la moitié des parties quand il a une profondeur de 5
Sur ma configuration alphabeta de profondeur 5 réfléchi 0.30 secondes par partie... c'est très court. Avec une profondeur de 8 elle s'améliore quand même avec une différence de taux de victoire qui augmente de 10 (--résultat : 40 défaite vs 60 victoires) et le temps devient à peine plus important (0.46 secondes)~~
Réglé grâce au fait que j'ai pu voir que le nombre d'appel à jouer n'était vraiment pas bon pour alphabeta...

Pb  :Maintenant il faudrait changer la fonction d'évaluation

# Conclusion :

J'ai eu un peu de mal a me revisualiser le fonctionnement de l'algo alphabeta mais je pense avoir réussi à l'implémenter.

Cependant pour tester l'efficacité des fonction d'évalutation il faut enchainer les tests mais cela prend beaucoup de temps ...
En effet pour une profondeur de 5 on peux tester, le temps d'exécution reste correct mais du coup l'IA n'est pas très efficace contre le random. Avec une profondeur de 8 je n'ai pas vraiment pu tester étant donner le temps d'éxécution énorme ne serait-ce que pour une seule partie.
Du coup j'ai à peine pu constater le résutat de moins de 10 parties, mais ce n'est pas très représentatif... Pour le alphabeta c'est pareil on voit quand même que le temps d'execution est plus faible donc c'est un bon point. Cependant pareil qu'avec minmax c'est vraiment difficile de tester un grand nombre de partie.

Au sujet des fonctions d'évalutation je n'ai pas vraiment pu faire de test non plus j'ai juste pu remarquer que la 1ere qui se base sur la différence de score n'est pas vraiment efficace.

J'ai par ailleurs remarqué que alphabeta et minmax ont le même comportement et donc donnent le même résultat. C'est parfaitement logique mais j'ai pu visualiser cette similitude en interchangeant les IA dans le choix de la couleur.

J'ai aussi remarqué que pour faire des gros score l'IA (avec fonction d'eval1) essaye de partir à gauche pour récupérer toute la colonne de gauche. Cela lui permet d'avoir toute une colonne pour bloquer le jeu de l'adversaire et récupérer ses jetons à chaque coup jusqu'a l'extremité droite du plateau.
Parfois cette technique fonctionne donc l'IA obtient vraiment un gros score et d'autres fois l'IA random contre son plan et réussi à gagner sans stratégie... Je pense donc qu'il faut travailler sur les fonctions d'évaluation pour améliorer l'IA pour qu'elle puisse peut être réfléchir d'une autre façon.
