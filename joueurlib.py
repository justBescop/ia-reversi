#!/usr/bin/python
# coding: utf8


import interfacelib as ilib
import numpy as np
import random as ra
import time

class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass



class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0
		

class Random(IA):
	def demande_coup(self):
		liste=self.jeu.plateau.liste_coups_valides(ilib.couleur_to_couleurval(self.couleur))
		
		if len(liste)==0:
			return []
		
		
		return ra.choice(liste)
		

class Minmax(IA):
	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.profondeurArbre = 5
		self.nbjouer=0
		
		# On récupère la couleur avec laquelle on commence l'arbre
		self.couleurMax=ilib.couleur_to_couleurval(couleur)
	#Fonction d'évaluation
	def evalPosition(self,plateau):
		score=plateau.score()
		return(score[0]-score[1])	
				
	#demander coup qui explore l'arbre	
	def exploration(self,plateau,profondeur,couleurJ):
		#Si on est à la fin de ce qu'on doit voir
		if profondeur>self.profondeurArbre:
			return [self.evalPosition(plateau),[]] # on retourne le score
		#sinon on regarde la liste des coups valides
		listeCoup = plateau.liste_coups_valides(couleurJ)
		#si on est sur une feuille
		if len(listeCoup)==0:
			return [self.evalPosition(plateau),[]] #on retourne le score
		if couleurJ == self.couleurMax: #dans le cas ou le joueur est le joueur MAX
			M=-1000
			for coup in listeCoup:
				#on va voir la suite de l'arbre
				tempP= plateau
				tempP.jouer(coup,couleurJ*-1)
				self.nbjouer+=1
				iteration = self.exploration(tempP.copie(),profondeur+1,couleurJ*-1)[0]
				if M < iteration : #si on a une meilleure val
					M = iteration
					meilleurCoup = coup #on garde le coup
			return (M,meilleurCoup)
		else:
			m=1000
			for coup in listeCoup:
				tempP= plateau
				tempP.jouer(coup,couleurJ*-1)
				self.nbjouer+=1
				iteration = self.exploration(tempP.copie(),profondeur+1,couleurJ*-1)[0]
				if m > iteration :#si on a un pire score
					m = iteration
					pireCoup = coup
			return (m,pireCoup)
				
	def demande_coup(self):
		self.nbjouer+=1
		#print(self.nbjouer)
		return self.exploration(self.jeu.plateau.copie(),1,self.couleurMax)[1]

class AlphaBeta(IA):
	
	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie,couleur,opts)
		self.profondeurArbre = 5
		self.coutJeton=[[10,5,5,5,5,5,5,10],[5,1,1,1,1,1,1,5],[5,1,1,1,1,1,1,5],[5,1,1,1,1,1,1,5],[5,1,1,1,1,1,1,5],[5,1,1,1,1,1,1,5],[5,1,1,1,1,1,1,5],[10,5,5,5,5,5,5,10]]
		self.nbjouer=0
		# On récupère la couleur avec laquelle on commence l'arbre
		self.couleurMax=ilib.couleur_to_couleurval(couleur)
	
	#Fonction d'évaluation --Différence de score
	def evalPosition(self,plateau):
		score=plateau.score()
		return(score[0]-score[1])
	
	#Fonction d'évaluation --Cout des cases différentes
	def evalPosition2(self,plateau):
		score=0
		for i in range(self.jeu.plateau.taille):
			for j in range(self.jeu.plateau.taille):
				score+= plateau.tableau_cases[i][j] * self.coutJeton[i][j]
		return score
			
	#demander coup qui explore l'arbre	
	def exploration(self,plateau,profondeur,couleurJ,alpha=-10000,beta=10000):
		#Si on est à la fin de ce qu'on doit voir
		if profondeur>self.profondeurArbre:
			return [self.evalPosition2(plateau),[]] # on retourne le score
		#sinon on regarde la liste des coups valides
		listeCoup = plateau.liste_coups_valides(couleurJ)
		#si on est sur une feuille
		if len(listeCoup)==0:
			return [self.evalPosition2(plateau),[]] #on retourne le score
		if couleurJ == self.couleurMax: #dans le cas ou le joueur est le joueur MAX
			M=-1000
			for coup in listeCoup:
				#on va voir la suite de l'arbre
				tempP= plateau
				tempP.jouer(coup,couleurJ*-1)
				self.nbjouer+=1
				iteration = self.exploration(tempP.copie(),profondeur+1,couleurJ*-1,alpha,beta)[0]
				if M < iteration : #si on a une meilleure val
					M = iteration
					meilleurCoup = coup #on garde le coup
					alpha= max(alpha,M)
					if alpha >= beta :
						break
			return (M,meilleurCoup)
		else: #dans le cas ou on regarde le joueur MIN
			m=1000
			for coup in listeCoup:
				tempP= plateau
				tempP.jouer(coup,couleurJ*-1)
				self.nbjouer+=1
				iteration = self.exploration(tempP.copie(),profondeur+1,couleurJ*-1,alpha,beta)[0]
				if m > iteration :#si on a un pire score
					m = iteration
					pireCoup = coup
					beta = min(beta,m)
					if alpha >= beta:
						break
			return (m,pireCoup)
				
	def demande_coup(self):
		self.nbjouer+=1
		print(self.nbjouer)
		return self.exploration(self.jeu.plateau.copie(),1,self.couleurMax)[1]

